const mercadopagoService = require('../services/mercadopagoService')
const URL = require('../constants/URL')

async function mercadoPagoRequest(req, res) {
    const mpLinkData = req.body
    console.log(mpLinkData)

    try {
        const data_preference_id = await mercadopagoService.mercadoPagoRequest(mpLinkData)
        return res.status(200).send(data_preference_id)
    }
    catch (error) {
        console.log(error)
        return res.status(401).send(error.message)
    }
}

async function hasMPAcc(req, res) {

    const idUser = req.body.idUser

    try {
        const existeCuentaMP = await mercadopagoService.hasMPAcc(idUser)
        return res.status(200).send(existeCuentaMP)
    }
    catch (error) {
        console.log(error)
        return res.status(500).send(error.message)
    }
}


async function linkMPAccount(req, res) {

    // se llamaria al abrir una pagina. i.e. 
    // https://auth.mercadopago.com.ar/authorization?client_id=6535661077233352&response_type=code&platform_id=mp&state=1&redirect_uri=http://gabriel-4a23db52.localhost.run/asociarcuentaMP

    const data = {
        code: req.query.code, // este es el access token
        state: req.query.state // el estado. se puede poner el idusuario
    }

    try {
        const mpUserData = await mercadopagoService.linkMPAccount(data)
        return res.status(200).redirect(URL.DOMAINFRONT+'/newpost')
    }
    catch (error) {
        console.log(error)
        return res.status(500).send(error.message)
    }

}


function recibido(req, res) {
    // es llamado una vez que se realiza un pago con mercadopago para ver si los datos son correctos de nuestro lado
    if (req.body.action == 'payment.created') {
        res.status(200).send("OK")
    }
    else {
        return res.status(500).send("No se completó el pago")
    }
}

module.exports = { mercadoPagoRequest, linkMPAccount, recibido, hasMPAcc};