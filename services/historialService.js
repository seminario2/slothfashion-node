const historialDAC = require('../dataaccess/historialDAC')
const validators = require('../utils/validators')


async function getHistorial(userId) {
    // validaciones
    try {
        validators.isGreaterThanZero(userId, "No es un post válido")
        const compras = await historialDAC.getHistorial(userId)
        const publicaciones = await historialDAC.getPublicacionesByUser(userId)
        const historial = { "compras": compras, "publicaciones": publicaciones }

        return historial
    }
    catch (error) {
        return Promise.reject(error)
    }
}

module.exports = { getHistorial }